(function() {
    'use strict';
    angular
        .module('askAltucher')
        .service('$user', user);

    user.$inject = [];

    function user() {
    	var status ={
    		loggedIn : false,
            userName : ''
    	};
        
    	this.status = status;
        this.userNav = {show:false};
        this.capitalizeFirstLetter = capitalizeFirstLetter;

        function capitalizeFirstLetter(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }
    }
})();
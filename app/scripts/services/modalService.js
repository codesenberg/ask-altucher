(function() {
    'use strict';
    angular
        .module('askAltucher')
        .service('$modalService', modal);

    modal.$inject = ['$modal'];

    function modal($modal) {
    	this.open = open;

        var modalTypes = {
                            basic:{
                                url:'views/modal-basic.html',
                                alias: 'modalBasic'
                            }
                        },
        data = {content: '', breakParse: '\n\n', title:''},
        modalInstance;

        function open(size, content, title, type, breakParse){
            data.content = content;
            type = (angular.isDefined(type)) ? type : 'basic';             
            if(angular.isDefined(breakParse)){
                data.breakParse = breakParse;
            }
            if(angular.isDefined(title)){
                data.title = title;
            }

            modalInstance = $modal.open({
              templateUrl: modalTypes[type].url,
              controller: 'ModalCtrl',
              controllerAs: modalTypes[type].alias,
              size: size,
              resolve: {
                data: function () {
                  return data;
                }
              }
          });
        }
    }
})();
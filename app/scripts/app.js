  'use strict';

/**
 * @ngdoc overview
 * @name askAltucher
 * @description
 * # askAltucher
 *
 * Main module of the application.
 */
angular
  .module('askAltucher', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngStorage',
    'ui.bootstrap',
    'ngMessages',
    'ui.calendar'
  ])
  .config(function ($routeProvider, $locationProvider) {
     //$locationProvider.hashPrefix('!');
    $routeProvider
      .when('/', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl as login'
      })
      .when('/daily', {
        templateUrl: 'views/daily.html',
        controller: 'DailyCtrl as daily'
      })
      .when('/register', {
        templateUrl: 'views/register.html',
        controller: 'RegisterCtrl as register'
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl as login'
      })
      .when('/contact', {
        templateUrl: 'views/contact.html',
        controller: 'ContactCtrl as contact'
      })
      .when('/appearances', {
        templateUrl: 'views/appearances.html',
        controller: 'AppearancesCtrl as appearances'
      })
      .when('/recos', {
        templateUrl: 'views/recos.html',
        controller: 'RecosCtrl as recos'
      })
      .when('/podcast', {
        templateUrl: 'views/podcast.html',
        controller: 'PodcastCtrl as podcast'
      })
      .when('/newsfeed', {
        templateUrl: 'views/newsfeed.html',
        controller: 'NewsfeedCtrl as newsfeed'
      })
      .otherwise({
        redirectTo: '/'
      });

      /*
      if (window.history && window.history.pushState) {
          $locationProvider.html5Mode({
                 enabled: true,
                 requireBase: false
          });
      }
      $locationProvider.html5Mode(true);*/
  });

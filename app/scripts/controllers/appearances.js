'use strict';
/**
 * @ngdoc function
 * @name askAltucher.controller:AppearancesCtrl
 * @description
 * # AppearancesCtrl
 * Controller of the askAltucher
 *
 * Also as user navigates between dates, days that dont have data can be removed from storage array
 */
(function() {
    'use strict';
    angular.module('askAltucher').controller('AppearancesCtrl', AppearancesCtrl);
    AppearancesCtrl.$inject = ['$scope', '$localStorage', '$user'];

    function AppearancesCtrl($scope,  $localStorage, $user) {
        $user.userNav.show = true;
        var vm = this;
        vm.upcoming = [];
        vm.previous = [];

        vm.upcoming.push({
            date: 1429782780000,
            location: 'Toronto, ON',
            info: 'Indigo Bookstore – 7pm'
        });
        vm.upcoming.push({
            date: 1426585980000,
            location: '',
            info: 'Choose Yourself! podcast Interview with Zee Smith from podcasts-R-Us'
        });
        vm.upcoming.push({
            date: 1425376380000,
            location: 'San Francisco, CA',
            info: 'TEDx'
        });
    }
})();
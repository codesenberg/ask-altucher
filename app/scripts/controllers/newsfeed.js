'use strict';
/**
 * @ngdoc function
 * @name askAltucher.controller:NewsfeedCtrl
 * @description
 * # NewsfeedCtrl
 * Controller of the askAltucher
 *
 * Also as user navigates between dates, days that dont have data can be removed from storage array
 */
(function() {
    'use strict';
    angular.module('askAltucher').controller('NewsfeedCtrl', NewsfeedCtrl);
    NewsfeedCtrl.$inject = ['$scope', '$localStorage', '$user'];

    function NewsfeedCtrl($scope, $localStorage, $user) {
        $user.userNav.show = true;
        var vm = this;

        vm.blogpost = [];
        vm.twitter = [];
        vm.quora = [];
        vm.user = $user.status.userName;
        vm.status = '';
        vm.submitForm = submitForm;
        vm.goBack = goBack;

        vm.blogpost.push({
            date: 1423475580000,
            title: 'What I Learned from my First Podcast in 1980…'
        });
        vm.blogpost.push({
            date: 1422870780000,
            title: 'The Ultimate Guide to Making a Personal Manifesto'
        });
        vm.blogpost.push({
            date: 1423475580000,
            title: 'What I Learned About Life After Interviewing ..'
        });

        function submitForm(){
            vm.status = 'signedup';
            window.scrollTo(0, 0);
        }

        function goBack(){
            vm.status = ''; 
            window.scrollTo(0, 0); 
        }
    }
})();
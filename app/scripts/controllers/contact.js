'use strict';
/**
 * @ngdoc function
 * @name askAltucher.controller:ContactCtrl
 * @description
 * # ContactCtrl
 * Controller of the askAltucher
 *
 * Also as user navigates between dates, days that dont have data can be removed from storage array
 */
(function() {
    'use strict';
    angular.module('askAltucher').controller('ContactCtrl', ContactCtrl);
    ContactCtrl.$inject = ['$scope', '$localStorage', '$user'];

    function ContactCtrl($scope, $localStorage, $user) {
        $user.userNav.show = true;
        var vm = this;
        vm.submitForm = submitForm;

     	function submitForm(form, v){
     		vm.formSubmitted = true;
     		vm[form] = '';
            window.scrollTo(0, 0);
     	}
    }
})();
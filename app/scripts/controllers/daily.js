'use strict';
/**
 * @ngdoc function
 * @name askAltucher.controller:DailyCtrl
 * @description
 * # DailyCtrl
 * Controller of the askAltucher
 *
 * Also as user navigates between dates, days that dont have data can be removed from storage array
 */
(function() {
    'use strict';
    angular.module('askAltucher').controller('DailyCtrl', DailyCtrl);
    DailyCtrl.$inject = ['$scope', '$localStorage', '$timeout', 'uiCalendarConfig', '$user'];

    function DailyCtrl($scope, $localStorage, $timeout, uiCalendarConfig, $user) {
        $user.userNav.show = true;
        var vm = this,
            myDate = moment(new Date),
            dateMonth = moment(new Date),
            dateFormat = 'MMM D, YYYY',
            dateFormatMonth = 'MMMM YYYY',
            listTitles = ['mental', 'physical', 'emotional', 'spiritual'];
        vm.date = myDate.format(dateFormat);
        vm.dateMonth = dateMonth.format(dateFormatMonth);
        vm.curView = 'day';
        vm.nextView = {
            val: 'month',
            label: 'Monthly'
        };
        vm.currentTab = [{
            active: true
        }, {
            active: false
        }, {
            active: false
        }, {
            active: false
        }];
        vm.months = [];
        vm.$storage = $localStorage.$default({
            dailyPage: {
                dates: {}
            }
        });
        vm.changeDate = changeDate;
        vm.changeView = changeView;
        vm.selectMonth = selectMonth;
        vm.changeDateMonth = changeDateMonth;
        initDate();
        createMonths();
        /**
         * Increments/subtracts date by 1
         * @param Boolean optional m defines direction of increment; default or true increments, false subtracts
         */
        function changeDate(m) {
            m = (angular.isDefined(m)) ? m : true;
            if (m) {
                vm.date = myDate.add(1, 'days').format(dateFormat);
            } else {
                vm.date = myDate.subtract(1, 'days').format(dateFormat);
            }
            initDate();
        }

        function changeDateMonth(m) {
            m = (angular.isDefined(m)) ? m : true;
            if (m) {
                vm.dateMonth = dateMonth.add(1, 'month').format(dateFormatMonth);
            } else {
                vm.dateMonth = dateMonth.subtract(1, 'month').format(dateFormatMonth);
            }
            vm.uiConfig.calendar.month = Number(dateMonth.format('M')) - 1;
        }
        /**
         * If list property is found in storage, apply value or else initialize empty list array
         * @param String v defines name of property
         */
        function buildList(v) {
            if (angular.isDefined($localStorage.dailyPage.dates[vm.date][v])) {
                vm[vm.date][v] = $localStorage.dailyPage.dates[vm.date][v];
            } else {
                vm[vm.date][v] = $localStorage.dailyPage.dates[vm.date][v] = {
                    item: []
                };
                for (var i = 0; i < 10; i++) {
                    vm[vm.date][v].item.push('');
                }
            }
        }
        /**
         * Initializes each list based on selected date or retrieves exist list from localStorage
         */
        function initDate() {
            if (!angular.isDefined(vm[vm.date])) {
                vm[vm.date] = {}
            }
            if (!angular.isDefined($localStorage.dailyPage.dates[vm.date])) {
                $localStorage.dailyPage.dates[vm.date] = {};
            }
            for (var i = 0; i < listTitles.length; i++) {
                buildList(listTitles[i]);
            }
        }
        /**
         * Changes view from daily, to monthly, to annually
         * @param string v expecting either 'day','month' or 'year'
         */
        function changeView(v) {
            if (v === 'month') {
                vm.nextView = {
                    val: 'year',
                    label: 'Year'
                };
                createCalendarEvents();
            }
            if (v === 'year') {
                vm.nextView = {};
            }
            vm.curView = v;
            window.scrollTo(0, 0);
        }

        function createMonths() {
            var createMonthsMoment = moment(new Date);
            for (var i = 0; i < 12; i++) {
                vm.months.push(createMonthsMoment.month(i).format('MMM'));
            }
        }

        function selectMonth(v) {
            vm.uiConfig.calendar.month = v;
            dateMonth = moment({
                year: dateMonth.format('YYYY'),
                month: v
            });
            vm.dateMonth = dateMonth.format(dateFormatMonth);
            changeView('month');
        }

        function createCalendarEvents() {
            /* can be optimized to have vm.events not always built on the fly;
             * it can be built initially then updated as user adds/removes;
             * also another property could be added to each object to signify that it has been modified and that it's not all empty strings
             */
            vm.events = [];
            angular.forEach($localStorage.dailyPage.dates, function(o, i) {
                angular.forEach(o, function(j, k) {
                    for (var l = 0; l < j.item.length; l++) {
                        if (j.item[l]) {
                            vm.events.push({
                                title: k,
                                start: moment(i)._d,
                                className: k + '-note'
                            });
                            break;
                        }
                    }
                });
            });
            vm.eventSources = [vm.events];
        }

        function eventClicked(date, jsEvent, view) {
            vm.date = moment(date._start).format(dateFormat);
            vm.currentTab[listTitles.indexOf(date.title)].active = true;
            changeView('day');
            initDate();
        }
        /* config object */
        vm.uiConfig = {
            calendar: {
                height: 450,
                editable: false,
                header: false,
                fixedWeekCount: false,
                eventClick: eventClicked,
                dayClick: function(date, jsEvent, view) {
                    vm.date = moment(date).format(dateFormat);
                    vm.currentTab[listTitles.indexOf('mental')].active = true;
                    changeView('day');
                    initDate();
                }
            }
        };
    }
})();
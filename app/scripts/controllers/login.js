'use strict';
/**
 * @ngdoc function
 * @name askAltucher.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the askAltucher
 *
 * Also as user navigates between dates, days that dont have data can be removed from storage array
 */
(function() {
    'use strict';
    angular.module('askAltucher').controller('LoginCtrl', LoginCtrl);
    LoginCtrl.$inject = ['$scope', '$localStorage', '$user', '$location'];

    function LoginCtrl($scope, $localStorage, $user, $location) {
        var vm = this;
        var users = (angular.isDefined($localStorage.registerPage)) ? $localStorage.registerPage.users : null;

        vm.submitForm = submitForm;
        vm.login = login;
        vm.signup = signup;
        vm.loginErrorAdvanced = '';
        vm.submitStatus  = 'start';

        function submitForm(){
            if(users !== null){
                for(var i = 0; i < users.length; i++){
                    if(vm.userName.toLowerCase() === users[i].email.toLowerCase() ){
                        if(vm.password === users[i].password){
                            vm.submitStatus = 'complete';
                            $user.status.loggedIn = true;
                            $user.status.userName = $user.capitalizeFirstLetter(users[i].email);
                            window.location.href='#daily';        
                        }else{
                            vm.loginErrorAdvanced = 'Incorrect password';
                        }
                        break;   
                    }else{
                        vm.loginErrorAdvanced = "User does not exist";
                    }
                }
            }
        }

        function login(){
            vm.submitStatus = 'login';
            window.scrollTo(0, 0);
        }

        function signup(){
            window.location.href='#register';
        }

    }
})();
'use strict';
/**
 * @ngdoc function
 * @name askAltucher.controller:PodcastCtrl
 * @description
 * # PodcastCtrl
 * Controller of the askAltucher
 *
 * Also as user navigates between dates, days that dont have data can be removed from storage array
 */
(function() {
    'use strict';
    angular.module('askAltucher').controller('PodcastCtrl', PodcastCtrl);
    PodcastCtrl.$inject = ['$scope', '$timeout', '$user', '$modalService'];

    function PodcastCtrl($scope, $timeout, $user, $modalService) {
        $user.userNav.show = true;
        var vm = this;
        var firstPlay = true;
        vm.james = [];
        vm.ask = [];
        vm.like = like;
        vm.chooseYourself = [];
        vm.playPodcast = playPodcast;
        vm.playPause = playPause;
        vm.goBack = goBack;
        vm.volume = volume;
        vm.playerStatus = {play:false, sound:true};
        vm.showInfo = showInfo;

        vm.ask.push({
            date: 1417002300000,
            episode: 218,
            title: 'How Important Is Suffering?',
            info: 'James says suffering is extremely important, yet it\'s always horrible. But a lot of people believe that through their suffering, they\'ve paid their dues.\n\nJames doesn\'t see it this way.\n\nIt\'s important to learn from your suffering. If you just suffer from your mistakes, but you don\'t learn anything from them, then you haven\'t gotten anywhere.\n\nLearn from your suffering.',
            views: 55,
            comments: 10,
            liked: false,
            podcast: 'podcasts/ask/podcast218.mp3',
            podcastOgg: 'podcasts/ask/podcast218.ogg'
        });
        vm.ask.push({
            date: 1416823680000,
            episode: 217,
            title: 'How one man makes six figures with Uber',
            info: 'Uber is popping up everywhere and it\'s a lot more than just a new way to hire a ride.\n\nJon Youshaei todays guest on Ask Altucher, shares his great story first published in Forbes about the "Uberpreneur" Gavin Escolar.',
            views: 78,
            comments: 17,
            liked: false,
            podcast: 'podcasts/ask/podcast217.mp3',
            podcastOgg: 'podcasts/ask/podcast217.ogg'
        });
        vm.ask.push({
            date: 1416304380000,
            episode: 216,
            title: 'Where Can Your Kids Publish Their Content',
            info: 'Today James does the show in Spanish... Just kidding.\n\nRoma wrote to James asking him where his daughter can publish her work. Whether it\'s a written piece, a video, or an audio recording, it doesn\'t matter.\n\nThere\'s so much opportunity for kids to get millions of views today using the Internet.\n\nIf you\'re a writer, then you can post your work on Fanfiction.net and Wattpad.com.\n\nIf you like to produce videos, then YouTube is the place to post your work. And of course, if audio is your thing, then do a podcast.\n\nEncourage your kids, give them positive reinforcement, and help them bring put their voice.\n\nP.S. Nicole Lapin\'s new book Rich Bitch hits the bookstores yesterday.\n\nNicole, known for being the youngest anchor ever at CNN and then going on to claim the same title at CNBC, joined James on today\'s special edition of Ask Altucher.\n\nNobody handed Nicole anything. She\'s worked her butt off since she was 15.\n\nShe started in Kentucky, then South Dakota, and went on to Palm Springs while still finishing her college degree.\n\nShe finally got a 4 a.m. time slot on CNBC and immediately fell in love with it.\n\nIn Nicole\'s new book Rich Bitch, she lays out a 12-step planin which she shares her experiences – mistakes and all – of getting her own finances in order. Money is typically an "off limits" conversation, but nothing is off limits here.',
            views: 80,
            comments: 25,
            liked: false,
            podcast: 'podcasts/ask/podcast216.mp3',
            podcastOgg: 'podcasts/ask/podcast216.ogg'
        });
         
        vm.james.push({
            date: 1417002300000,
            episode: 65,
            title: 'Dan Ariely: Dishonesty, Irrationality, and Money',
            info: 'This week on The James Altucher Show, I am joined by Dan Ariely.\n\nWhen Dan was a teenager, he was severely burned on over 70% of his body. He then spent the next three years in the hospital enduring agonizing pain.\n\nHis experience in the hospital led him down the path of exploring irrational behaviors. He wanted to understand how to better deliver painful and unavoidable treatments to patients.\n\nAfter completing this initial research project, he became engrossed with the idea that people repeatedly and predictably make the wrong decisions in many aspects of their lives and that research could help change some of these patterns.\n\nHis interests span a wide range of behaviors, and his sometimes unusual experiments are consistently interesting, amusing and informative, demonstrating profound ideas that fly in the face of common wisdom.\n\nIt’s a great interview that I know you will enjoy!',
            views: 50,
            comments: 1,
            liked: false,
            podcast: 'podcasts/james-show/podcast65.mp3',
            podcastOgg: 'podcasts/james-show/podcast65.ogg'
        });
        vm.james.push({
            date: 1416823680000,
            episode: 64,
            title: 'Tucker Max: Surprise Announcement! Part 1',
            info: 'My good friend Tucker Max was my first ever guest on The James Altucher Show. Now, 63 episodes later, he is my first repeat guest.\n\nA few months ago, Tucker let me in on a secret that he had been keeping. He told me that he wanted to wait a while before he let the public know what it was, but once he decided to, he would like to reveal it on my podcast.\n\nAnd he sure as hell delivers.\n\nI’d like to talk more about the episode and the secret that he reveals, but I don’t want to ruin the surprise. Tune into the latest episode to find out what Tucker Max’s secret is!',
            views: 75,
            comments: 7,
            liked: false,
            podcast: 'podcasts/james-show/podcast64.mp3',
            podcastOgg: 'podcasts/james-show/podcast64.ogg'
        });
        vm.james.push({
            date: 1416304380000,
            episode: 63,
            title: 'Tony Robbins: Money Is Just A Game',
            info: 'Tony Robbins has a new book out today. For the first time in 25 years I think. So, Claudia and I went down to meet with him so I could interview him for The James Altucher Show.\n\nI was really excited to fly down to his house in Florida and interview him. It was like going down to visit a member of the X-Men.\n\nClaudia and I got to his house and his staff helped us set up our equipment and then we took a walk out to the ocean. It was beautiful. We felt lucky and relaxed.',
            views: 100,
            comments: 12,
            liked: false,
            podcast: 'podcasts/james-show/podcast63.mp3',
            podcastOgg: 'podcasts/james-show/podcast63.ogg'
        });

        function like(o, status){
            status = angular.isDefined(status) ? status : true;
            if(status){
                o.liked = true;
            }else{
                o.liked = false;
            }
        }

        function playPodcast(v, o, t, views){
            vm.currentTitle = t;
            vm.currentPodcast = v;
            vm.currentPodcastOgg = o;
            vm.currentViews = views;
            window.scrollTo(0, 0);
        }

        function playPause(){
          if(!vm.playerStatus.play){
                $('#podcastPlayer')[0].play();
                vm.playerStatus.play = true;
                if(firstPlay){
                    vm.currentViews.views++;
                    firstPlay = false;
                } 
          }else{
                $('#podcastPlayer')[0].pause(); 
                vm.playerStatus.play = false;
          }
         
        }

        function volume(){
            if(!vm.playerStatus.sound){
                $timeout(function(){
                    $('#podcastPlayer')[0].muted = false;
                    
                });
                vm.playerStatus.sound = true;
            }else{
                $timeout(function(){
                    $('#podcastPlayer')[0].muted = true;
                });
                vm.playerStatus.sound = false;
            }    
        }

        function goBack(){
            firstPlay = true; 
            vm.currentPodcast=false;
            vm.playerStatus.play = false;
            vm.playerStatus.sound = true;
            window.scrollTo(0, 0);
        }
       // 'This week on The James Altucher Show, I am joined by Dan Ariely.\n\nWhen Dan was a teenager, he was severely burned on over 70% of his body. He then spent the next three years in the hospital enduring agonizing pain.\n\nHis experience in the hospital led him down the path of exploring irrational behaviors. He wanted to understand how to better deliver painful and unavoidable treatments to patients.\n\nAfter completing this initial research project, he became engrossed with the idea that people repeatedly and predictably make the wrong decisions in many aspects of their lives and that research could help change some of these patterns.\n\nHis interests span a wide range of behaviors, and his sometimes unusual experiments are consistently interesting, amusing and informative, demonstrating profound ideas that fly in the face of common wisdom.\n\nIt’s a great interview that I know you will enjoy!\n\nThis is a preview of Ep. 65: Dan Ariely: Dishonesty, Irrationality, and Money.'

        function showInfo(info){
            $modalService.open('sm', info, 'Info');
        }
    }
})();
'use strict';

/**
 * @ngdoc function
 * @name askAltucher.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the askAltucher
 */

(function() {
    'use strict';
    angular.module('askAltucher').controller('MainCtrl', MainCtrl);
    MainCtrl.$inject = ['$scope', '$localStorage', '$window', '$user'];

    function MainCtrl($scope, $localStorage, $window, $user) {
        //$localStorage.$reset();
        var vm = this;
        vm.status = $user.status
        vm.navToggle = navToggle;
        vm.userNav = $user.userNav;

        function navToggle(){
            if($('#ham-nav').hasClass('open')){
                $('#ham-list').css('display','none');
                $('#ham-nav').removeClass('open');
                $('#ham-nav').css('background', 'transparent');
                $('#ham-nav-open').css('display', 'block');
                $('#ham-nav-close').css('display', 'none');
            }else{
                $('#ham-list').css('display','block');
                $('#ham-nav').addClass('open');
                $('#ham-nav-open').css('display', 'none');
                $('#ham-nav').css('background', 'gray');
                $('#ham-nav-close').css('display', 'block');
            }
        }

    }
})();
'use strict';
/**
 * @ngdoc function
 * @name askAltucher.controller:ModalCtrl
 * @description
 * # ModalCtrl
 * Controller of the askAltucher
 *
 * Also as user navigates between dates, days that dont have data can be removed from storage array
 */
(function() {
    'use strict';
    angular.module('askAltucher').controller('ModalCtrl', ModalCtrl);
    ModalCtrl.$inject = ['$scope', '$modalInstance', 'data'];

    function ModalCtrl($scope, $modalInstance, data) {
        var vm = this;
        vm.contentBlocks = data.content.split('\n\n');
        vm.title = data.title;
        vm.close = close;

        function close(){
            $modalInstance.close();
        }
    }
})();
'use strict';
/**
 * @ngdoc function
 * @name askAltucher.controller:RecosCtrl
 * @description
 * # RecosCtrl
 * Controller of the askAltucher
 *
 * Also as user navigates between dates, days that dont have data can be removed from storage array
 */
(function() {
    'use strict';
    angular.module('askAltucher').controller('RecosCtrl', RecosCtrl);
    RecosCtrl.$inject = ['$scope', '$localStorage', '$user'];

    function RecosCtrl($scope, $localStorage, $user) {
        $user.userNav.show = true;
        var vm = this;
        vm.james = [];
        vm.recos = [];

        vm.james.push({
            date: 1425513600000,
            title: 'The Power of No',
            author: 'At Vero',
            info: 'Drawing on their own stories as well as feedback from their readers and students, James Altucher and Claudia Azula Altucher clearly show that you have the right to say no: To anything that is hurting you. To standards that no longer serve you. To people who drain you of your creativity and expression. To beliefs that are not true to the real you. When you do, you’ll be freed to say a truly powerful “Yes” in your life—one that opens the door to opportunities, abundance, and love.',
            image: 'images/books/thepowerofno.jpg'
        });
        vm.james.push({
            date: 1431302400000,
            title: 'The Choose Yourself Stories',
            author: 'Eos Et Accusamus',
            info: 'These are the raw, best-written stories of James Altucher as he rides the roller coaster of wealth, poverty, abundance, romance, tragedy, comedy, and everything in between. From the depths of despair to revelation and honesty, these stories are James at his best in writing and rawness. He kept these stories under wraps until now.',
            image: 'images/books/chooseyourselfstories.jpg'
        });
        vm.james.push({
            date: 1435276800000,
            title: 'Choose Yourself',
            author: 'Et Iusto',
            info: 'The world is changing. Markets have crashed. Jobs have disappeared. Industries have been disrupted and are being remade before our eyes. Everything we aspired to for “security,” everything we thought was “safe,” no longer is: College. Employment. Retirement. Government. It’s all crumbling down. In every part of society, the middlemen are being pushed out of the picture. No longer is someone coming to hire you, to invest in your company, to sign you, to pick you. It’s on you to make the most important decision in your life: Choose Yourself.',
            image: 'images/books/chooseyourself_sm.jpg'
        });

         vm.recos.push({
            date: 1425513600000,
            title: 'Zero to One: Notes on Startups, or How to Build the Future',
            author: 'by Peter Thiel, 2014',
            url: 'http://www.amazon.com/Zero-One-Notes-Startups-Future/dp/0804139296/ref=sr_1_1?s=books&ie=UTF8&qid=1425260655&sr=1-1&keywords=zero+to+one',
            image: 'images/books/zero-to-one.jpg'
        });
        vm.recos.push({
            date: 1431302400000,
            title: 'The Rise of Superman: Decoding the Science of Ultimate Human Performance ',
            author: 'by Steven Kotler, 2014 ',
            url: 'http://www.amazon.com/Rise-Superman-Decoding-Ultimate-Performance/dp/1477800832/ref=sr_1_1?s=books&ie=UTF8&qid=1425260552&sr=1-1&keywords=rise+of+superman',
            image: 'images/books/rise-of-superman.jpg'
        });
        vm.recos.push({
            date: 1435276800000,
            title: 'MONEY Master the Game: 7 Simple Steps to FInancial Freedom',
            author: 'by Tony Robbins, 2014',
            url: 'http://www.amazon.com/MONEY-Master-Game-Financial-Freedom/dp/1476757801/ref=sr_1_1?s=books&ie=UTF8&qid=1425260719&sr=1-1&keywords=tony+robbins',
            image: 'images/books/money-master-the-game.jpg'
        });
    }
})();
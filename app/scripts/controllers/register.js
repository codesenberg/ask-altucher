'use strict';

/**
 * @ngdoc function
 * @name askAltucher.controller:RegisterCtrl
 * @description
 * # RegisterCtrl
 * Controller of the askAltucher
 */

 (function() {
    'use strict';
    angular
        .module('askAltucher')
        .controller('RegisterCtrl', RegisterCtrl)
        .directive('unique', unique);

    RegisterCtrl.$inject = ['$scope', '$localStorage', '$user', '$timeout'];

    function RegisterCtrl($scope, $localStorage, $user, $timeout) {
 		 var vm = this;
 		 vm.submitForm = submitForm;
 		 vm.userNameFormatted = '';

 		 vm.$storage = $localStorage.$default({
			    registerPage: {
			    	users: []
			    }
		});

 		 vm.ageOptions = [
		    {ageGroup: '18-25'},
		    {ageGroup: '25-35'},
		    {ageGroup: '35-45'},
		    {ageGroup: '45+'}
		  ];
		  vm.employmentOptions = [
		    {employment: 'Employed'},
		    {employment: 'Self Employed'},
		    {employment: 'Unemployed'},
		    {employment: 'Student'}
		  ];

		  function submitForm(){
		  	vm.$storage.registerPage.users.push({
		  		uname: vm.userName,
		  		fname: vm.firstName,
		  		lname: vm.lastName,
		  		email: vm.email,
		  		sex: vm.sex,
		  		age: vm.age,
		  		employment: vm.employment,
		  		password: vm.password
		  	});
		  	$user.status.loggedIn = true;
		  	$user.status.userName = vm.userNameFormatted = $user.capitalizeFirstLetter(vm.userName);

		  	vm.complete = true;
		  	window.scrollTo(0, 0);
		  	$timeout(function(){

		  		window.location.href='#daily';
		  	},3000);
		  }
    }

    function unique(){
    	var directive = {
            require: 'ngModel',
            restrict: 'A',
            link: uniqueLink
        };

        return directive;

        function uniqueLink(scope, element, attrs, ctrl) {
        	var users = scope.register.$storage.registerPage.users,
        	    uniqueType;
        	switch(attrs.name){
        		case 'userName':
        			uniqueType = 'uname';
        		break;
        		case 'email':
        			uniqueType = 'email';
        		break;
        	}
            ctrl.$validators.unique = function(modelValue) {         	
            	if(angular.isDefined(modelValue)){
	            	for(var i=0; i < users.length; i++){
	            		if(users[i][uniqueType].toLowerCase() === modelValue.toLowerCase()){
	            			return false;
	            		}
	            	}
	            }
            	return true;
            }
        }
    }
})();
